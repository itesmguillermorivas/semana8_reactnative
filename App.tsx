import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Text, View, Image, Button, TextInput, ToastAndroid, ScrollView, FlatList } from 'react-native';

// cómo hacer request 
const request = async () => {

  var response = await fetch("https://bitbucket.org/itesmguillermorivas/partial2/raw/45f22905941b70964102fce8caf882b51e988d23/carros.json");

  var json = await response.json();

  console.log(json);
  console.log(json[0]);
  console.log(json[0].marca);
}

const alertita = (mensaje:string) => {

  alert(mensaje);
  console.log(mensaje);
  request();
  //ToastAndroid.show(mensaje, ToastAndroid.SHORT);
}

// podemos crear todos los componentes que queramos
const Perrito = (props:any) => {

  // props - argumentos que recibimos del exterior y personalizan el componente
  // estados - variables internas de un componente que pueden cambiar
  const[estaFeliz, setEstaFeliz] = useState(false);
  const[textito, setTextito] = useState("");

  return(
    <View>
      <Text>woof woof! {props.nombre} y estoy {estaFeliz? "FELIZ! :D" : "TRISTE :("}</Text>
      <Text>{props.saludo}</Text>
      <Button
        title="cambiar estado de animo"
        onPress={() => {
          setEstaFeliz(!estaFeliz);
        }}
      />
      <TextInput 

        placeholder="introduce texto"
        onChangeText={text => {
          setTextito(text)
        }}
      />
      <Text>woof! {textito}</Text>
      <Image 
        source={{uri: props.uri}}
        style={{width: 100, height: 100}}
      />
      <Button 
        title="mostrar estado de animo"
        onPress={() => {

          alertita(estaFeliz + "");
        }}
      />
    </View>    
  );
}

const App = () => {

  // ESTO ES JSX
  return (
    <View style={styles.container}>
      <Text>Hola amiguitos!</Text>
      <FlatList 
        data={[
          {nombre: "chucho1", saludo: "hola1", uri: "http://4.bp.blogspot.com/-7R8yr5qhV8Q/UPFlZvJCB4I/AAAAAAAAI7s/f-CYwM5F6k4/s1600/puppy.jpg"},
          {nombre: "chucho2", saludo: "hola2", uri: "http://4.bp.blogspot.com/-7R8yr5qhV8Q/UPFlZvJCB4I/AAAAAAAAI7s/f-CYwM5F6k4/s1600/puppy.jpg"},
          {nombre: "chucho3", saludo: "hola3", uri: "http://4.bp.blogspot.com/-7R8yr5qhV8Q/UPFlZvJCB4I/AAAAAAAAI7s/f-CYwM5F6k4/s1600/puppy.jpg"},
          {nombre: "chucho4", saludo: "hola4", uri: "http://4.bp.blogspot.com/-7R8yr5qhV8Q/UPFlZvJCB4I/AAAAAAAAI7s/f-CYwM5F6k4/s1600/puppy.jpg"},
          {nombre: "chucho5", saludo: "hola5", uri: "http://4.bp.blogspot.com/-7R8yr5qhV8Q/UPFlZvJCB4I/AAAAAAAAI7s/f-CYwM5F6k4/s1600/puppy.jpg"}
        ]}
        renderItem={({item}) => 
          <Perrito 
            nombre={item.nombre}
            saludo={item.saludo}
            uri={item.uri}
          />
        }
      />
      <ScrollView>
        <Perrito 
          nombre="Firulais" 
          saludo="Hola" 
          uri="http://4.bp.blogspot.com/-7R8yr5qhV8Q/UPFlZvJCB4I/AAAAAAAAI7s/f-CYwM5F6k4/s1600/puppy.jpg"
        />
        <Perrito 
          nombre="Fifi" 
          saludo="Saludos" 
          uri="http://4.bp.blogspot.com/-7R8yr5qhV8Q/UPFlZvJCB4I/AAAAAAAAI7s/f-CYwM5F6k4/s1600/puppy.jpg"
        />
        <Perrito 
          nombre="Killer" 
          saludo="Buenas tardes" 
          uri="http://4.bp.blogspot.com/-7R8yr5qhV8Q/UPFlZvJCB4I/AAAAAAAAI7s/f-CYwM5F6k4/s1600/puppy.jpg"
        />
      </ScrollView>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default App;